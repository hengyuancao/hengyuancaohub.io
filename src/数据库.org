#+TITLE: 数据库
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="./themes/css/main.css"/>


* SQL语句
- 演示数据
|----+----------+---------------------------+-------+---------|
| id | name     | url                       | alexa | country |
|----+----------+---------------------------+-------+---------|
|  1 | Google   | https://www.google.cm/    |     1 | USA     |
|  2 | 淘宝     | https://www.taobao.com/   |    13 | CN      |
|  3 | 菜鸟教程 | http://www.runoob.com/    |  4689 | CN      |
|  4 | 微博     | http://weibo.com/         |    20 | CN      |
|  5 | Facebook | https://www.facebook.com/ |     3 | USA     |

** sql selsct 语句
#+BEGIN_SRC sql
select <column_name>,<column_name>
from <table_name>;
与
select * from table_name;
#+END_SRC
** sql select distinct 语句
#+BEGIN_SRC sql
select distinct <column_name>,<column_name>
from <table_name>;
#+END_SRC
** sql where 语句
#+BEGIN_SRC sql
SELECT * FROM Websites WHERE country='CN';
SELECT * FROM Websites WHERE id=1;
#+END_SRC
where语句中的运算符
| =	    | 等于                                                   |
| <>	   | 不等于。注释：在 SQL 的一些版本中，该操作符可被写成 != |
| >	    | 大于                                                   |
| <	    | 小于                                                   |
| >=	   | 大于等于                                               |
| <=	   | 小于等于                                               |
| BETWEEN      | 	在某个范围内                                   |
| LIKE	 | 搜索某种模式                                           |
| IN	   | 指定针对某个列的多个可能值                             |
** sql and & or 运算符
#+BEGIN_SRC sql
SELECT * FROM Websites
WHERE country='CN'
AND alexa > 50;

#+END_SRC
** sql order by 语法
#+BEGIN_SRC sql
select * from Websites
order by alexa;
#降序
select * from Websites
order by alexa desc;
#+END_SRC
** SQL INSERT INTO 语法
#+BEGIN_SRC sql
INSERT INTO 语句可以有两种编写形式。
第一种形式无需指定要插入数据的列名，只需提供被插入的值即可：
INSERT INTO table_name
VALUES (value1,value2,value3,...);

第二种形式需要指定列名及被插入的值：
INSERT INTO table_name (column1,column2,column3,...)
VALUES (value1,value2,value3,...);
#+END_SRC
* MySQL
1. mysql 基本操作
| selsct version();                      | 显示当前MySQL的version信息       |
| show global variables like 'port';     | 查看MySQL端口号                  |
| show databases;                        | 显示所有能操作的数据库           |
| create database new_base charset=utf8; | 创建数据库,并设置编码格式为utf-8 |
| use new_base;                          | 使用new_base数据库               |
| drop new_base;                         | 删除数据库                       |
2. 操作表
    - 首先使用<use new_base;>进入new_base数据库
    - 创建数据表结构
#+BEGIN_SRC sql
create table new_table(

         id#<字段名称>   int#<字段类型>                          unsigned auto_increment not null primary key,#<字段约束>
         name            varchar(20)                             default '',
         age             tinyint                                 unsigned default 0,
         height          decimal(5,2)                            ,
         gender          enum('男','女','中性','保密')   default '保密',
         cls_id          int                                     unsigned default 0,
         is_delete       bit                                     default 0);
#+END_SRC
    - 其它字段类型
      - 日期类型
	| date      | 3字节 | 日期，格式:2014-09-18              |
	| time      | 3字节 | 时间，格式:08:42:30                |
	| datetime  | 8字节 | 日期时间，格式:2014-09-18 08:42:30 |
	| timestamp | 4字节 | 自动储存记录修改的时间             |
	| year      | 1字节 | 年份                               |
      - 数值类型
	| MySQL数据类型	  | 字节数	 | 含义(带有符号)                 |
	|------------------------+----------------+--------------------------------|
	| tinyint	        | 	       | 1字节	范围（-128~127）  |
	| smallint	       | 2字节	  | 范围（-32768~32767）           |
	| mediumint	      | 3字	    | 范围（-8388608~8388607）       |
	| int		    | 4字节	  | 范围（-2147483648~2147483647） |
	| bigint		 | 8字节	  | 范围（+-9.22*10的18次方）      |
	| float(m, d)	    | 4字节	  | 单精度浮点型，m总个数，d小数位 |
	| double(m, d)	   | 8字节	  | 双精度浮点型，m总个数，d小数位 |
	| decimal(m, d)	  | 	       | decimal是存储为字符串的浮点数  |
      - 字符类型
	| char(n)	      | 固定长度，最多255个字符         |
	| varchar(n)	   | 可变长度，最多65535个字符       |
	| tinytext	     | 可变长度，最多255个字符         |
	| text		 | 可变长度，最多65535个字符       |
	| mediumtext	   | 可变长度，最多2的24次方-1个字符 |
	| longtext	     | 可变长度，最多2的32次方-1个字符 |
    - 其它约束条件
	| primary key      	 | 主键约束:一般指自增，唯一，非空的序号 |
	| foreign key      	 | 外键约束:关联另外一张表的字段；       |
	| unique           	 | 唯一约束:值唯一（null 可以有多个）    |
	| not null         	 | 非空约束:不能为空                     |
	| check            	 | 检查约束:限制字段内容                 |
	| default           	    | 默认约束:初始化值；                   |
	| enum()                    |                                       |
	| unsigned                  |                                       |
	|                           |                                       |
3. 插入数据
#+BEGIN_SRC sql
#全列插入
insert into new_table values
(0,'小明',18,180.00,2,1,0),
(0,'小月月',18,180.00,2,2,1),
(0,'彭于晏',29,185.00,1,1,0),
(0,'刘德华',59,175.00,1,2,1);
#部分插入
insert into new_table(id,name) values(3,"xiao");
#+END_SRC
4. 修改数据
#+BEGIN_SRC sql
updata new_table set name='da' where id=5;
#+END_SRC
5. 查询数据
#+BEGIN_SRC sql
#查询new_table中所有列
select * from new_table;
#查询指定列
select id,name from new_table;

#+END_SRC
6.删除数据
#+BEGIN_SRC sql
delete from new_table where id=8;
#+END_SRC
